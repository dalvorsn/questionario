<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Usuario;
use Faker\Generator as Faker;

$factory->define(Usuario::class, function (Faker $faker) {
    return [
        'login' => $faker->unique()->word,
        'nome' => $faker->name,
        'senha' => $faker->password,
    ];
});
