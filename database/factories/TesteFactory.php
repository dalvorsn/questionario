<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Teste;
use Faker\Generator as Faker;

$factory->define(Teste::class, function (Faker $faker) {
    $min = $faker->numberBetween(40, 80);
    $max = $faker->numberBetween($min, 150);
    return [
        'id_usuario' => $faker->unique()->numberBetween(1, 10),
        'nome' => $faker->words($faker->numberBetween(2, 5), true),
        'pontuacao_minima' => $min,
        'pontuacao_maxima' => $max,
    ];
});
