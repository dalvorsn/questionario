<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Questao;
use Faker\Generator as Faker;

$factory->define(Questao::class, function (Faker $faker) {
    $data = [
        'id_teste' => $faker->numberBetween(1, 10),
        'enunciado' => $faker->words($faker->numberBetween(5, 10), true) . '?',
        'respostaA' => $faker->words($faker->numberBetween(1, 3), true),
        'respostaB' => $faker->words($faker->numberBetween(1, 3), true),
        'respostaC' => $faker->words($faker->numberBetween(1, 3), true),
        'respostaD' => $faker->words($faker->numberBetween(1, 3), true),
        'respostaE' => $faker->words($faker->numberBetween(1, 3), true),
    ];
    
    $data['correta'] = $faker->randomElement(['A', 'B', 'C', 'D', 'E']);

    return $data;
});
