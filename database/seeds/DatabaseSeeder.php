<?php

use App\Model\Usuario;
use App\Model\Teste;
use App\Model\Questao;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Usuario::class, 10)->create();
        factory(Teste::class, 10)->create();
        factory(Questao::class, 150)->create();
    }
}
