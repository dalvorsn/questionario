<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', "LoginController@index")->name('login');
Route::post('login', "LoginController@doLogin")->name('doLogin');
Route::get('register', "LoginController@register")->name('register');
Route::post('register', "LoginController@doRegister")->name('doRegister');
Route::get('logout', "LoginController@logout")->name('logout');

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

    Route::get('myTests', 'TesteController@showMyTests')->name('myTests');
    Route::resource('teste', 'TesteController', [ 'except' => [ 'show' ] ]);
    Route::resource('teste.questao', 'QuestaoController', [ 'except' => [ 'index', 'show' ]]);
    Route::get('responder/{teste}', "QuestaoRespostaController@create")->name('responseTest');
    Route::post('responder/{teste}', "QuestaoRespostaController@store")->name('storeResponseTest');
    Route::get('result/{teste}', "QuestaoRespostaController@showResult")->name('testResult');
});
