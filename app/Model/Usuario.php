<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Usuario extends User
{
    protected $fillable = [
        'nome',
        'login',
        'senha'
    ];

    /**
     * Get all of the posts for the country.
     */
    public function tests()
    {
//        return $this->hasMany(Teste::class);//, Usuario::class, 'id');
        return $this->hasMany(Teste::class, 'id_usuario');
    }
}
