<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuestaoResposta extends Model
{
    protected $table = 'questoes_respostas';
    protected $fillable = [
        'id_usuario',
        'id_questao',
        'resposta'
    ];
}
