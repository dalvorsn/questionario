<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Teste extends Model
{
    protected $fillable = [
        'id_usuario',
        'nome',
        'pontuacao_minima',
        'pontuacao_maxima'
    ];

    public function owner()
    {
        return $this->hasOne(Usuario::class, 'id', 'id_usuario');
    }

    public function questions()
    {
        return $this->hasMany(Questao::class,  'id_teste');
    }

    public function answers() {
        return $this->hasManyThrough(
            QuestaoResposta::class,
            Questao::class,
            'id_teste',
            'id_questao',
            'id',
            'id'
        );
    }
}
