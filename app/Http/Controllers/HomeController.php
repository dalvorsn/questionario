<?php

namespace App\Http\Controllers;

use App\Model\Questao;
use App\Model\QuestaoResposta;
use App\Model\Teste;
use App\Model\Usuario;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stats = (object)[
          "users" => Usuario::all()->count(),
          "tests" => Teste::all()->count(),
          "anwsers" => QuestaoResposta::all()->count(),
          "questions" => Questao::all()->count()
        ];

        return view('pages.home')->with('stats', $stats);
    }
}
