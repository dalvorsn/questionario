<?php

namespace App\Http\Controllers;

use App\Model\QuestaoResposta;
use App\Model\Teste;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestaoRespostaController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $teste = Teste::findOrFail($id);
        $teste->load('questions');
        $user = Auth::user();

        $anwsers = $teste->answers->where('id_usuario', $user->id);
        if(count($anwsers) > 0) {
            return redirect()->route('testResult', $id);
        }

        return view('pages.response.create')->with('teste', $teste)->with('alternativas', QuestaoController::alternativas);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $teste = Teste::findOrFail($id);
        $teste->load('questions');

        $user = Auth::user();
        $anwsers = $teste->answers->where('id_usuario', $user->id);
        if(count($anwsers) > 0) {
            return redirect()->route('testResult', $id);
        }

        $data = $request->all();

        foreach ($teste->questions as $question) {
            $anwser = $data['response-' . $question->id];
            QuestaoResposta::create(["id_usuario" => $user->id, "id_questao" => $question->id, "resposta" => $anwser]);
        }

        return redirect()->route('testResult', $id);
    }

    public function showResult($id) {
        $teste = Teste::findOrFail($id);
        $teste->load('questions');

        $user = Auth::user();
        $anwsers = $teste->answers->where('id_usuario', $user->id);

        $acertos = 0;
        foreach ($teste->questions as $question) {
            foreach ($anwsers as $anwser) {
                if($anwser->id_questao == $question->id) {
                    if($question->correta == $anwser->resposta) {
                        $acertos++;
                    }
                    continue;
                }
            }
        }

        $ret = (object)[
            "id" => $id,
            "acertos" => $acertos,
            "questoes" => count($teste->questions),
            "nota" => $teste->pontuacao_maxima / count($teste->questions) * $acertos,
            "notaMinima" => $teste->pontuacao_maxima,
            "notaMaxima" => $teste->pontuacao_maxima,
        ];

        return view('pages.response.result')->with('result', $ret);
    }
}
