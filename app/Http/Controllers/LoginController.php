<?php

namespace App\Http\Controllers;

use App\Model\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        if(Auth::check()) {
            return redirect()->route('home');
        }

        return view('pages.login');
    }

    public function doLogin(Request $request)
    {
        if(Auth::check()) {
            return redirect()->route('home');
        }

        if(!Auth::attempt($request->only(['login', 'senha']))) {
            return back()->with('errorMessage', 'Login ou senha incorretos');
        }

        return redirect()->route('home');
    }

    public function register()
    {
        if(Auth::check()) {
            return redirect()->route('home');
        }

        return view('pages.register');
    }

    public function doRegister(Request $request)
    {
        if(Auth::check()) {
            return redirect()->route('home');
        }

        if (Usuario::where('login', $request->login)->first()) {
            return back()->with('errorMessage', 'Usuário inválido.');
        }

        $data = $request->only(['login', 'nome', 'senha']);

        Usuario::create($data);
        return redirect()->route('login')->with('success', 'Usuário criado com sucesso!');
    }

    public function logout() {
        Auth::logout();

        return redirect()->route('login');
    }
}
