<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Questao;
use App\Model\Teste;


class QuestaoController extends Controller
{
    const alternativas = ['A', 'B', 'C', 'D', 'E'];
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($testeId)
    {
        $teste = Teste::findOrFail($testeId);
        return view('pages.questao.create')->with('teste', $teste)->with('alternativas', self::alternativas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($testeId, Request $request)
    {
        $teste = Teste::findOrFail($testeId);
        $data = $request->all();

        Questao::create($data);
        return redirect()->route('teste.edit', $teste->id)->with('success', 'Questão adicionada com sucesso.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($testeId, $id)
    {
        $teste = Teste::findOrFail($testeId);
        $questao = Questao::findOrFail($id);
        return view('pages.questao.edit')->with('teste', $teste->id)->with('questao', $questao)->with('alternativas', self::alternativas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($testeId, $id, Request $request)
    {
        $teste = Teste::findOrFail($testeId);
        $questao = Questao::findOrFail($id);

        $data = $request->all();
        $questao->update($data);
        return redirect()->route('teste.edit', $teste->id)->with('success', 'Questão alterada com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($testeId, $id)
    {
        $teste = Teste::findOrFail($testeId);
        $questao = Questao::findOrFail($id);
        $questao->delete();
        return redirect()->route('teste.edit', $teste->id)->with('success', 'Questão removida com sucesso.');
    }
}
