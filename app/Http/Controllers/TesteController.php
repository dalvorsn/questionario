<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Teste;
use Illuminate\Support\Facades\Auth;

class TesteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testes = Teste::all();
        $testes->load('owner');

        return view('pages.teste.index')->with('testes', $testes);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showMyTests()
    {
        $user = Auth::user();
        $testes = $user->tests;
        $testes->load('owner');

        return view('pages.teste.index')->with('testes', $testes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.teste.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $data['id_usuario'] = $user->id;

        Teste::create($data);
        return redirect()->route('teste.index')->with('success', 'Teste inserido com sucesso');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teste = Teste::findOrFail($id);
        $teste->load('questions');

        return view('pages.teste.edit')->withTeste($teste);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user();
        $teste = Teste::findOrFail($id);
        if($teste->owner != $user) {
            return redirect()->route('teste.index')->with('errorMessage', 'Teste não pode ser modificado, pois não é de sua autoria.');
        }

        $teste->update($data);
        return redirect()->route('teste.index')->with('success', 'Teste atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $teste = Teste::findOrFail($id);
        if($teste->owner != $user) {
            return redirect()->route('teste.index')->with('errorMessage', 'Teste não pode ser excluido, pois não é de sua autoria.');
        }

        $teste->delete();
        return redirect()->route('teste.index')->with('success', 'Teste removido com sucesso.');
    }
}
