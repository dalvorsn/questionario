<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>@yield('pageName')</title>

<!-- global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

<!-- core js files -->
<script src="{{ asset('assets/js/main/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/main/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
<!-- /core js files -->

<!-- theme js files -->
<script src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

<script src="{{ asset('assets/js/app.js') }}"></script>
<script src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>


<!-- /theme js files -->
