<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            footer
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    © Copyright 2020 Mateus & Dalvo
                </span>
    </div>
</div>
