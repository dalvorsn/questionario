<!doctype html>
<html lang="en">
<head>
    @include('includes.head')
</head>

<body>

    @auth
        <!-- main navbar -->
        @include('includes.navbar')
        <!-- /main navbar -->
    @endauth

<!-- page content -->
<div class="page-content">

    @auth
        <!-- main sidebar -->
        @include('includes.sidebar')
        <!-- /main sidebar -->
    @endauth

    <!-- main content -->
    <div class="content-wrapper">
    @auth
        <!-- page header -->
        @include('includes.header')
        <!-- /page header -->
    @endauth

        <!-- content area -->
        <div class="content">
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ \Session::get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (\Session::has('errorMessage'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ \Session::get('errorMessage') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif


            @auth
                    <div class="card">
                        <div class="card-body">
                            @yield('content')
                        </div>
                    </div>
            @endauth
            @guest
                <div class="content d-flex justify-content-center align-items-center">
                    @yield('content')
                </div>
            @endguest
        </div>
        <!-- /content area -->

    @auth
        <!-- footer -->
        @include('includes.footer')
        <!-- /footer -->
    @endauth

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->
</body>
</html>
