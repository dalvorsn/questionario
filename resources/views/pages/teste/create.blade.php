@extends('layouts.default')
@section('pageName', 'Teste - Criar')
@section('content')
<form action="{{route('teste.store')}}" method="post">
    @csrf
    <div class="form-group">
        <label for="nome">Nome</label>
        <input class="form-control" name="nome" id="nome" value="{{old('nome')}}" required>
    </div>
    <div class="form-group">
        <label for="pontuacaoMinima">Pontuação Minima</label>
        <input class="form-control" name="pontuacao_minima" id="pontuacaoMinima" value="{{old('pontuacao_minima')}}" required>
    </div>
    <div class="form-group">
        <label for="pontuacaoMaxima">Pontuação Máxima</label>
        <input class="form-control" name="pontuacao_maxima" id="pontuacaoMaxima" value="{{old('pontuacao_maxima')}}" required>
    </div>

    <button class="btn btn-lg btn-success">Inserir</button>
</form>
@stop
