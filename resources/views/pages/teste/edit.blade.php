@extends('layouts.default')
@section('pageName', 'Teste - Editar')
@section('content')
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Atenção</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseja remover esta pergunta?
            </div>
            <div class="modal-footer">
                <form id="formDeletePergunta" action="" method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Remover</button>
                </form>
            </div>
        </div>
    </div>
</div>
<form action="{{route( 'teste.update', $teste->id ) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="nome">Nome</label>
        <input class="form-control" name="nome" id="nome" value="{{ $teste->nome }}">
    </div>
    <div class="form-group">
        <label for="pontuacaoMinima">Pontuação Minima</label>
        <input class="form-control" name="pontuacao_minima" id="pontuacaoMinima" value="{{ $teste->pontuacao_minima }}" required>
    </div>
    <div class="form-group">
        <label for="pontuacaoMaxima">Pontuação Máxima</label>
        <input class="form-control" name="pontuacao_maxima" id="pontuacaoMaxima" value="{{ $teste->pontuacao_maxima }}">
    </div>
    <table class="table">
        <thead>
            <tr>
                <td colspan="2">
                    <a class="btn btn-success float-right text-white" role="button" href="{{ route('teste.questao.create', [ "teste" => $teste->id ]) }}">
                        <i class="fa fa-plus" aria-hidden="true"></i> Novo
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col">Pergunta</th>
                <th scope="col" class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
        @forelse($teste->questions as $questao)
            <tr>
                <td>{{ $questao->enunciado }} </td>
                <td class="text-center">
                    <div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="{{ route('teste.questao.edit', [ "questao" => $questao->id, "teste" => $teste->id ]) }}" class="dropdown-item"><i class="icon-database-edit2"></i> Editar questão</a>
                                <a class="dropdown-item deleteButton" data-toggle="modal" data-target="#deleteModal" data-endpoint="{{ route('teste.questao.destroy', [ "questao" => $questao->id, "teste" => $teste->id ]) }}">
                                    <i class="icon-trash" aria-hidden="true"></i> Remover questão
                                </a>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="2">Não há perguntas cadastrados.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    <button class="btn btn-success">Atualizar</button>
    <button form="formDelete" class="btn btn-danger">Remover</button>
</form>

<form id="formDelete" action="{{ route('teste.destroy', $teste->id) }}" method="POST">
    @csrf
    @method('DELETE')
</form>
<script>
    window.addEventListener('load', function() {
        $('.deleteButton').click(function() {
            $route = $(this).data('endpoint');
            $('#formDeletePergunta').attr('action', $route);
        });
    });
</script>
@stop
