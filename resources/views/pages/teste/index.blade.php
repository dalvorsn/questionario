@extends('layouts.default')
@section('pageName', 'Teste - Listar')
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col" class="text-center">#</th>
            <th scope="col">Nome</th>
            <th scope="col">Autor</th>
            <th scope="col" class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse($testes as $teste)
            <tr>
                <th scope="row" class="text-center">{{ $teste->id }}</th>
                <td> {{ $teste->nome }}</td>
                <td> {{ $teste->owner->nome }}</td>
                <td class="text-center">
                    <div class="list-icons">
                        <div class="dropdown">
                            <a href="#" class="list-icons-item" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right">
                                @if(Auth::user()->id == $teste->owner->id)
                                    <a href="{{ route('teste.edit', $teste->id) }}" class="dropdown-item"><i class="icon-database-edit2"></i> Editar teste</a>
                                @endif
                                <a href="{{ route('responseTest', $teste->id) }}" class="dropdown-item"><i class="icon-bubble-dots4"></i> Responder teste</a>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4">Não há testes cadastrados.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    <div class="row">
        <div class="col-12 text-center">
            <a class="btn btn-success" href="{{ route('teste.create') }}" role="button">Inserir teste</a>
        </div>
    </div>

@endsection
