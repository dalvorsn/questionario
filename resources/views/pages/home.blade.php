@extends('layouts.default')
@section('content')
@section('pageName', 'Home')
    <div class="mb-3">
        <h6 class="mb-0 font-weight-semibold">
            Dashboard
        </h6>
        <span class="text-muted d-block">Estatisticas de uso</span>
    </div>
    <div class="row">
        <div class="col-sm-6 col-xl-3">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3 align-self-center">
                        <i class="icon-bookmark icon-3x text-success-400"></i>
                    </div>

                    <div class="media-body text-right">
                        <h3 class="font-weight-semibold mb-0">{{ $stats->tests }}</h3>
                        <span class="text-uppercase font-size-sm text-muted">total de testes</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-3">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3 align-self-center">
                        <i class="icon-question3 icon-3x text-indigo-400"></i>
                    </div>

                    <div class="media-body text-right">
                        <h3 class="font-weight-semibold mb-0">{{ $stats->questions }}</h3>
                        <span class="text-uppercase font-size-sm text-muted">total de perguntas</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-3">
            <div class="card card-body">
                <div class="media">
                    <div class="media-body">
                        <h3 class="font-weight-semibold mb-0">{{ $stats->anwsers }}</h3>
                        <span class="text-uppercase font-size-sm text-muted">total de respostas</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-book-play icon-3x text-blue-400"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-3">
            <div class="card card-body">
                <div class="media">
                    <div class="media-body">
                        <h3 class="font-weight-semibold mb-0">{{ $stats->users }}</h3>
                        <span class="text-uppercase font-size-sm text-muted">total de usuarios</span>
                    </div>

                    <div class="ml-3 align-self-center">
                        <i class="icon-user icon-3x text-danger-400"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
