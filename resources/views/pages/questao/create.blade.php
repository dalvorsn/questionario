@extends('layouts.default')
@section('pageName', 'Questão - Criar')
@section('content')
<form action="{{route('teste.questao.store', $teste->id)}}" method="POST">
    @csrf

    <input type="hidden" name="id_teste" value="{{ $teste->id }}">
    <div class="form-group">
        <label for="enunciado">Enunciado</label>
        <input class="form-control" name="enunciado" id="enunciado" value="{{old('enunciado')}}">
    </div>
    @foreach($alternativas as $a)
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="resposta{{ $a }}">{{ $a }}</span>
            </div>
            <input type="text" class="form-control" name="resposta{{ $a }}" value="{{ old('resposta' . $a) }}">
        </div>
    @endforeach
    <div class="form-group">
        <label for="correta">Resposta</label>
        <select class="form-control" id="correta" name="correta">
            @foreach($alternativas as $a)
                <option value="{{ $a }}" {{ ( old('correta') == $a) ? 'selected' : '' }} >{{ $a }}</option>
            @endforeach
        </select>
    </div>

    <button class="btn btn-success">Inserir</button>
</form>
@stop
