@extends('layouts.default')
@section('pageName', 'Questão - Editar')
@section('content')
<form action="{{route('teste.questao.update', ['questao' => $questao->id, 'teste' => $questao->id_teste])}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="enunciado">Enunciado</label>
        <input class="form-control" name="enunciado" id="enunciado" value="{{$questao->enunciado}}">
    </div>
    @foreach($alternativas as $a)
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="resposta{{ $a }}">{{ $a }}</span>
            </div>
            <input type="text" class="form-control" name="resposta{{ $a }}" value="{{ $questao->{'resposta' . $a} }}">
        </div>
    @endforeach
    <div class="form-group">
        <label for="correta">Resposta</label>
        <select class="form-control" id="correta" name="correta">
            @foreach($alternativas as $a)
                <option value="{{ $a }}" {{ ( $questao->correta == $a) ? 'selected' : '' }} >{{ ($a . ' - ' . $questao->{'resposta' . $a}) }}</option>
            @endforeach
        </select>
    </div>

    <button class="btn btn-success">Atualizar</button>
    <button form="formDelete" class="btn btn-danger">Remover</button>
</form>

<form id="formDelete" action="{{route('teste.questao.destroy', ['questao' => $questao->id, 'teste' => $questao->id_teste])}}" method="POST">
    @csrf
    @method('DELETE')
</form>
@stop
