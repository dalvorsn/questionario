@extends('layouts.default')
@section('pageName', 'Teste - Responder')
@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h3 class="card-title font-weight-black">{{ $teste->nome }}</h3>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col">
                    <span class="font-weight-black">Número de Questões:</span> {{ count($teste->questions) }}
                </div>
                <div class="col">
                    <span class="font-weight-black"> Pontuação Minima para aprovação: </span> {{ $teste->pontuacao_minima }}
                </div>
                <div class="col">
                    <span class="font-weight-black">Pontuação Máxima: </span> {{ $teste->pontuacao_maxima }}
                </div>
            </div>
        </div>
    </div>
    <form action="{{ route('storeResponseTest', $teste->id) }}" method="POST">
        @csrf

        <div class="row">

            @foreach($teste->questions as $question)
            <div class="col-6">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{ $loop->iteration }} - {{ $question->enunciado }}</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    @foreach($alternativas as $alternativa)
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" value="{{ $alternativa }}" name="response-{{ $question->id }}" {{ $loop->first ? 'checked' : '' }} required>
                                                {{ $alternativa }} - {{ $question->{'resposta' . $alternativa} }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
        <div class="row">
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-success">Finalizar teste</button>
            </div>
        </div>
    </form>
@stop
