@extends('layouts.default')
@section('pageName', 'Teste - Resultado')
@section('content')
    <div class="text-center">
        @if($result->nota >= $result->notaMinima)
            <i class="icon-book icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
            <h5 class="card-title">Aprovado!</h5>
        @else
            <i class="icon-book icon-2x text-danger-400 border-danger-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
            <h5 class="card-title">Reprovado!</h5>
        @endif
        <p class="mb-3">Você acertou {{ $result->acertos }} de {{ $result->questoes }} questões, sua nota foi: {{ $result->nota }} em {{ $result->notaMaxima }}</p>

        <a href="{{ route('teste.index') }}" class="btn bg-warning-400">Responder Outros</a>
    </div>
@stop
